import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DashboardComponent } from './component/dashboard/dashboard.component';
import { TotalComponent } from './component/total/total.component';
import { BranchComponent } from './component/branch/branch.component';
import { WeekRowComponent } from './component/week-row/week-row.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';


import { ButtonModule } from 'primeng/button';
import { CardModule, } from 'primeng/card'; 




@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    TotalComponent,
    BranchComponent,
    WeekRowComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    CardModule,
    ButtonModule,
    NgbModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
