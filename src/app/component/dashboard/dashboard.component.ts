import { Component, DoCheck, OnChanges, OnInit } from '@angular/core';
import { Status } from 'src/app/interface-model/status';
import {Week} from 'src/app/interface-model/week'

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})


export class DashboardComponent implements OnInit{

  constructor() { }

  //Variable Declaration ===============================================================
  public baseData: Status[] = [
    {used: this.generateUsedValue(50), available: 50}, 
    {used: this.generateUsedValue(80), available: 80},
    {used: this.generateUsedValue(100), available: 100}
  ]
  public title: string[] = ['Regular', 'Diesel', 'Premuim']
  public weekData: Week
  public weekLoopCount: number[] 
 
  ngOnInit(): void { 
    this.weekLoopCount = this.setLoopCount(5)  
  } 
 
  // TODO: Generate used value =========================================================
  private generateUsedValue (limit: number): number {
    return Math.floor(Math.random() * limit)
  }  

 
  //TODO: Generate an object of week row record ========================================
  generateWeekData(): Week { 
    let name = ""
    let regular = {used: this.generateUsedValue(50), available: 50}
    let diesel = {used: this.generateUsedValue(80), available: 80}
    let premuim = {used: this.generateUsedValue(100), available: 100}
    return {name, regular, diesel, premuim} 
  }
  

  //TODO: Just create a loop time in the component using *ngFor ========================
  private setLoopCount(loopAmount: number): number[]
  {  
    let count: number[]= []    
    for(let i=0; i<loopAmount; i++) 
    {   
      count.push(i)
    }      
    return count
  }  
}
 