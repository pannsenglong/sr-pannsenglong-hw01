import { Component, Input, OnChanges, OnInit } from '@angular/core';
import { Status } from 'src/app/interface-model/status';

@Component({
  selector: 'app-total',
  templateUrl: './total.component.html',
  styleUrls: ['./total.component.css']
})
export class TotalComponent implements OnInit{

  constructor() { }
 
  @Input() baseData: Status
  @Input() index: number 
  @Input() title: string[]
  
    
  ngOnInit(): void
  {     
  }   

 
 
}                