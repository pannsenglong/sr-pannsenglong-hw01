import { Component, Input, OnChanges, OnInit } from '@angular/core';
import { Week } from 'src/app/interface-model/week';

@Component({
  selector: '[app-week-row]',
  templateUrl: './week-row.component.html',
  styleUrls: ['./week-row.component.css']
})
export class WeekRowComponent implements OnInit {

  constructor() { }
  @Input() weekData: Week
  @Input() index: number 
  
  ngOnInit(): void { 
  }

}   
